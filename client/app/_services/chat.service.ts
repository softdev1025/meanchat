import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { appConfig } from '../app.config';
import { User, Chat } from '../_models/index';

@Injectable()
export class ChatService {
    constructor(private http: HttpClient) { 
        // this.getMessage();
    }

    getAll() {
        return this.http.get<User[]>(appConfig.apiUrl + '/users');
    }

    getById(_id: string) {
        return this.http.get(appConfig.apiUrl + '/users/' + _id);
    }

    create(user: User) {
        return this.http.post(appConfig.apiUrl + '/users/register', user);
    }

    update(user: User) {
        return this.http.put(appConfig.apiUrl + '/users/' + user._id, user);
    }

    delete(_id: string) {
        return this.http.delete(appConfig.apiUrl + '/users/' + _id);
    }

    sendMessage(chat: Chat) {
        return this.http.post<any>(appConfig.apiUrl + '/chat/sendmessage', chat)
            .map(messages => {
                // console.log(messages)
            });
        
    }

    getMessage() {
        return this.http.post<any>(appConfig.apiUrl + '/chat/','');
    }
}