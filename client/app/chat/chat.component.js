"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var index_1 = require("../_services/index");
var ChatComponent = /** @class */ (function () {
    function ChatComponent(userService, chatService) {
        this.userService = userService;
        this.chatService = chatService;
        this.users = [];
        this.Messages = [];
        this.now = new Date();
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    }
    ChatComponent.prototype.ngOnInit = function () {
        this.loadAllUsers();
        this.getMessage();
    };
    ChatComponent.prototype.loadAllUsers = function () {
        var _this = this;
        this.userService.getAll().subscribe(function (users) { _this.users = users; });
    };
    ChatComponent.prototype.sendMessage = function () {
        var _this = this;
        this.chat = {
            user: this.currentUser.username,
            message: this.message,
            time: new Date()
        };
        this.chatService.sendMessage(this.chat)
            .subscribe(function (data) {
            console.log(data);
            _this.getMessage();
        }, function (error) {
            console.log(error);
        });
        // this.chatService.sendMessage(this.chat); 
        this.message = '';
    };
    ChatComponent.prototype.getMessage = function () {
        var _this = this;
        this.chatService.getMessage().subscribe(function (data) {
            _this.Messages = data;
            console.log(_this.Messages);
        }, function (error) {
            console.log(error);
        });
    };
    ChatComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'chat-view',
            templateUrl: 'chat.component.html'
        }),
        __metadata("design:paramtypes", [index_1.UserService, index_1.ChatService])
    ], ChatComponent);
    return ChatComponent;
}());
exports.ChatComponent = ChatComponent;
//# sourceMappingURL=chat.component.js.map