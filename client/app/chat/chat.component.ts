﻿import { Component, OnInit } from '@angular/core';

import { User, Chat } from '../_models/index';
import { UserService, ChatService } from '../_services/index';

@Component({
    moduleId: module.id,
    selector: 'chat-view',
    templateUrl: 'chat.component.html'
})

export class ChatComponent implements OnInit {
    currentUser: User;
    users: User[] = [];
    message:string;
    chat: Chat;
    Messages: Chat[] = [];
    public now: Date = new Date();

    constructor(private userService: UserService, private chatService: ChatService) {
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    }

    ngOnInit() {
        this.loadAllUsers();
        this.getMessage();
    }

    private loadAllUsers() {
        this.userService.getAll().subscribe(users => { this.users = users; });
    }

    sendMessage() {
        this.chat = {
            user: this.currentUser.username,
            message:this.message,
            time: new Date()
        };
        this.chatService.sendMessage(this.chat)
        .subscribe(
            data => {
                console.log(data)
                this.getMessage();
            },
            error => {
                console.log(error)
            });
        // this.chatService.sendMessage(this.chat); 
        this.message = '';
    }

    getMessage() {
        this.chatService.getMessage().subscribe(
            data => {
                this.Messages = data;
                console.log(this.Messages)
            },
            error => {
                console.log(error)
            }
        )
    }
}