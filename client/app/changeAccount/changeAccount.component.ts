﻿import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../_models/index';

import { AlertService, UserService } from '../_services/index';

@Component({
    moduleId: module.id,
    selector: 'change-account-view',
    templateUrl: 'changeAccount.component.html'
})

export class ChangeAccountComponent {
    model: any = {};
    loading = false;
    currentUser: User;

    constructor(
        private router: Router,
        private userService: UserService,
        private alertService: AlertService) { 
            this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        }

    update() {
        this.loading = true;
        console.log(this.model)
        this.model._id = this.currentUser._id;
        this.userService.update(this.model)
            .subscribe(
                data => {
                    this.alertService.success('Registration successful', true);
                    this.router.navigate(['/login']);
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }
}
