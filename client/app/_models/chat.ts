import { DatePipe } from "@angular/common/src/pipes/date_pipe";

export class Chat {
    user: string;
    message: string;
    time: Date;
}